# Face Similarity Game

## Purpose

This is a game to choose who looks most like a celebrity.

## How to play the game

### Preparation work:

- The players are seated together in 8 different tables. 
- Their photos are taken and placed into the `images` folders according to their table number (i.e. `table1` to `table8`).
- The filenames of the photos follows the players' name (e.g. `John_Tan.png`)
- Celebrities' photos are placed in `images\table0`.

### Start game:

- A celebrity is first chosen under the `Reference` dropdown.
  - The photo of the celebrity will be shown in the box.
- Each table is to discuss who among their own table resembles most like the celebrity.
  - The photo of that nominated person will be chosen from the dropdown (i.e. `Table 1` to `Table 8`) on the left panel. 
  - Their corresponding photos will be displayed.
- Once all 8 teams has made their choices, click `Run AI Model`.
- The similarity score will be calculated and displayed under each player.
- Once all the similarity scores are tabulated, the balloons will fly up.
- The player with the highest score will be boxed up in red.
- Continue the game with other celebrities.
- Note: Minimally, the celebrity's photo and at least 1 player's photo must be chosen to avoid `unhandled exception`.


## Installation

Run the following commands in terminal.

```
conda create -n proj_face python=3.9
conda activate proj_face

# Inside activated conda environment
pip install streamlit
pip install deepface
```

To run the game:
```
# cd into the folder

streamlit run app.py
```

Open up your browser to:
```
http://localhost:8501/
```

To end the game, press `Ctrl-C` in your terminal and close your browser.

Note:

- When run for the first time, `weights` will be downloaded into `C:\Users\your_username\.deepface\weights\vgg_face_weights.h5`. 
- This downloading may take some time.
- The AI algorithm is `deepface` from [https://github.com/serengil/deepface](https://github.com/serengil/deepface).