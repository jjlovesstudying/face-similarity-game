import streamlit as st
import os
from deepface import DeepFace
from PIL import Image, ImageOps
from pathlib import Path


def getNameFromFilename(filename):
    return Path(filename).stem.replace("_", " ").title()

def resizeImage(image_path):
    image = Image.open(image_path)
    #return image.resize((137,178))
    img = Image.open(image_path).resize((137,178)) 
    return ImageOps.expand(img,border=2,fill='black')

def resizeWinningImage(image_path):
    image = Image.open(image_path)
    #return image.resize((137,178))
    img = Image.open(image_path).resize((137,178)) 
    return ImageOps.expand(img,border=3,fill='red')

def resizeRefImage(image_path):
    image = Image.open(image_path)
    #return image.resize((137,178))
    img = Image.open(image_path).resize((237,278)) 
    return ImageOps.expand(img,border=2,fill='black')    

def getFileListInTable(table_name):
    filelist=["---"]
    for root, dirs, files in os.walk(f"./images/{table_name}"):
        for file in files:
            #filename=os.path.join(root, file)
            filelist.append(file)
    return filelist        
    

def convertDisimilarToSimilar(disimilar):
    similar = (1.0-disimilar)*100
    return round(similar,1)


def deepface_similarity(fileA_path, fileB_path):
    similarTemp = 0
    try:
        resultTemp = DeepFace.verify(img1_path=fileA_path, img2_path = fileB_path)
        distanceTemp = round(resultTemp["distance"], 4)
        similarTemp = convertDisimilarToSimilar(distanceTemp)
        #st.json(resultTemp)
    except:
        pass
    return similarTemp


def drawAvatarBox():
    st.image(resizeImage(f"./images/avatar_box.png"))
    
def drawRefAvatarBox():
    st.image(resizeRefImage(f"./images/avatar_box.png"))


#-----------------------------------------------------
# Streamlit Config
#-----------------------------------------------------
st.set_page_config(page_title="ACES Day", layout="wide")
st.title('Star Wars - The Stars Among Us')




# Remove menu at top right
hide_menu_style = """
        <style>
        #MainMenu {visibility: hidden; }
        footer {visibility: hidden;}
        </style>
        """
st.markdown(hide_menu_style, unsafe_allow_html=True)


# Remove menu panel
st.write('<style>div.block-container{padding-top:2rem;}</style>', unsafe_allow_html=True)


st.markdown("""
    <style>
        .stButton button {
            background: #1e81b0;
            border-radius: 8px;
            color: white;
            padding: 8px 18px;
            width: 100%;
            height: 50px;
            border: 10; 
            border-color: black;
            font-size: 55px;
        }
        .stButton button:hover {
            color: black;
            border-color: #1e81b0;
            background: white;
        }
        .stButton button:active {
            box-shadow: none;
            
            background: #abdbe3;    /*light blue*/
        }
        .stButton button:focus:not(:active) {
            background: #1e81b0;
            box-shadow: none;
            border-color: #1e81b0;
            color: white;
        }



    </style>
""", unsafe_allow_html=True)





#-----------------------------------------------------
# Sidebar
#-----------------------------------------------------

with st.sidebar:
    st.header("Choose photos")
    

    person0 = st.selectbox(
        'Reference:',
        getFileListInTable("table0"))
    
    person1 = st.selectbox(
        'Table 1',
        getFileListInTable("table1"))
    
    person2 = st.selectbox(
        'Table 2',
        getFileListInTable("table2"))
    
    person3 = st.selectbox(
        'Table 3',
        getFileListInTable("table3"))
    
    person4 = st.selectbox(
        'Table 4',
        getFileListInTable("table4"))

    person5 = st.selectbox(
        'Table 5',
        getFileListInTable("table5"))
    
    person6 = st.selectbox(
        'Table 6',
        getFileListInTable("table6"))
    
    person7 = st.selectbox(
        'Table 7',
        getFileListInTable("table7"))
    
    person8 = st.selectbox(
        'Table 8',
        getFileListInTable("table8"))
    


#-----------------------------------------------------
# Main Panel
#-----------------------------------------------------

with st.container():
    
    st.markdown("<h5> Reference </h5>", unsafe_allow_html=True)

    if person0 is not None and person0!="---":
        #st.image(f"./images/table0/{person0}")
        st.image(resizeRefImage(f"./images/table0/{person0}"))
    else:
        drawRefAvatarBox()

    
#-----------------------------------------------------
# Row 1 panel
#-----------------------------------------------------

with st.container():
    col1, col2, col3, col4 = st.columns(4)
  
    with col1:
        st.markdown("<h5> Table 1 </h5>", unsafe_allow_html=True)
        if person1 is not None and person1!="---":
            placeholderImage1 = st.image(resizeImage(f"./images/table1/{person1}"))
            placeholder1 = st.empty()
        else:
            drawAvatarBox()

    with col2:
        st.markdown("<h5> Table 2 </h5>", unsafe_allow_html=True)
        if person2 is not None and person2!="---":
            placeholderImage2 = st.image(resizeImage(f"./images/table2/{person2}"))
            placeholder2 = st.empty()
        else:
            drawAvatarBox()
            
    with col3:
        st.markdown("<h5> Table 3 </h5>", unsafe_allow_html=True)
        if person3 is not None and person3!="---":
            placeholderImage3 = st.image(resizeImage(f"./images/table3/{person3}"))
            placeholder3 = st.empty()
        else:
            drawAvatarBox()
                
    with col4:
        st.markdown("<h5> Table 4 </h5>", unsafe_allow_html=True)
        if person4 is not None and person4!="---":
            placeholderImage4 = st.image(resizeImage(f"./images/table4/{person4}"))
            placeholder4 = st.empty()
        else:
            drawAvatarBox()

#-----------------------------------------------------
# Row 2 panel
#-----------------------------------------------------
with st.container():
    col5, col6, col7, col8 = st.columns(4)

    with col5:
        st.markdown("<h5> Table 5 </h5>", unsafe_allow_html=True)
        if person5 is not None and person5!="---":
            placeholderImage5 = st.image(resizeImage(f"./images/table5/{person5}"))
            placeholder5 = st.empty()
        else:
            drawAvatarBox()
            
    with col6:
        st.markdown("<h5> Table 6 </h5>", unsafe_allow_html=True)
        if person6 is not None and person6!="---":
            placeholderImage6 = st.image(resizeImage(f"./images/table6/{person6}"))
            placeholder6 = st.empty()
        else:
            drawAvatarBox()
            
    with col7:
        st.markdown("<h5> Table 7 </h5>", unsafe_allow_html=True)
        if person7 is not None and person7!="---":
            placeholderImage7 = st.image(resizeImage(f"./images/table7/{person7}"))
            placeholder7 = st.empty()
        else:
            drawAvatarBox()
            
    with col8:
        st.markdown("<h5> Table 8 </h5>", unsafe_allow_html=True)
        if person8 is not None and person8!="---":
            placeholderImage8 = st.image(resizeImage(f"./images/table8/{person8}"))
            placeholder8 = st.empty()
        else:
            drawAvatarBox()




#-----------------------------------------------------
# Run AI
#-----------------------------------------------------
            
with st.container():

    if st.button(f":bulb: Run AI Model   "):

        file0 = f"./images/table0/{person0}"
        file1 = f"./images/table1/{person1}"
        file2 = f"./images/table2/{person2}"
        file3 = f"./images/table3/{person3}"
        file4 = f"./images/table4/{person4}"
        file5 = f"./images/table5/{person5}"
        file6 = f"./images/table6/{person6}"
        file7 = f"./images/table7/{person7}"
        file8 = f"./images/table8/{person8}"


        similiarHighest = -1
        tableHighest = 0

        # Calculate similiarity
        similar1 = deepface_similarity(file0, file1)
        if "placeholder1" in dir():
            placeholder1.metric(getNameFromFilename(person1), f"{similar1}%") 

        similar2 = deepface_similarity(file0, file2)
        if "placeholder2" in dir():
            placeholder2.metric(getNameFromFilename(person2), f"{similar2}%") 
        
        similar3 = deepface_similarity(file0, file3)
        if "placeholder3" in dir():
            placeholder3.metric(getNameFromFilename(person3), f"{similar3}%") 

        similar4 = deepface_similarity(file0, file4)
        if "placeholder4" in dir():
            placeholder4.metric(getNameFromFilename(person4), f"{similar4}%") 

        similar5 = deepface_similarity(file0, file5)
        if "placeholder5" in dir():
            placeholder5.metric(getNameFromFilename(person5), f"{similar5}%")   
        
        similar6 = deepface_similarity(file0, file6)
        if "placeholder6" in dir():
            placeholder6.metric(getNameFromFilename(person6), f"{similar6}%") 
        
        similar7 = deepface_similarity(file0, file7)
        if "placeholder7" in dir():
            placeholder7.metric(getNameFromFilename(person7), f"{similar7}%") 
        
        similar8 = deepface_similarity(file0, file8)
        if "placeholder8" in dir():
            placeholder8.metric(getNameFromFilename(person8), f"{similar8}%") 
        
        # Find the highest
        if similar1 > similiarHighest:
            similiarHighest = similar1
            tableHighest = 1
        if similar2 > similiarHighest:
            similiarHighest = similar2
            tableHighest = 2
        if similar3 > similiarHighest:
            similiarHighest = similar3
            tableHighest = 3
        if similar4 > similiarHighest:
            similiarHighest = similar4
            tableHighest = 4
        if similar5 > similiarHighest:
            similiarHighest = similar5
            tableHighest = 5
        if similar6 > similiarHighest:
            similiarHighest = similar6
            tableHighest = 6
        if similar7 > similiarHighest:
            similiarHighest = similar7
            tableHighest = 7
        if similar8 > similiarHighest:
            similiarHighest = similar8
            tableHighest = 8
            
        
        # Display Winner
        if tableHighest == 1:
            placeholder1.metric(getNameFromFilename(person1), f"{similar1}% <WIN!>")
            placeholderImage1.image(resizeWinningImage(f"./images/table1/{person1}"))
        elif tableHighest == 2:
            placeholder2.metric(getNameFromFilename(person2), f"{similar2}% <WIN!>")
            placeholderImage2.image(resizeWinningImage(f"./images/table2/{person2}"))
        elif tableHighest == 3:
            placeholder3.metric(getNameFromFilename(person3), f"{similar3}% <WIN!>")
            placeholderImage3.image(resizeWinningImage(f"./images/table3/{person3}"))
        elif tableHighest == 4:
            placeholder4.metric(getNameFromFilename(person4), f"{similar4}% <WIN!>")
            placeholderImage4.image(resizeWinningImage(f"./images/table4/{person4}"))
        elif tableHighest == 5:
            placeholder5.metric(getNameFromFilename(person5), f"{similar5}% <WIN!>")
            placeholderImage5.image(resizeWinningImage(f"./images/table5/{person5}"))
        elif tableHighest == 6:
            placeholder6.metric(getNameFromFilename(person6), f"{similar6}% <WIN!>")
            placeholderImage6.image(resizeWinningImage(f"./images/table6/{person6}"))
        elif tableHighest == 7:
            placeholder7.metric(getNameFromFilename(person7), f"{similar7}% <WIN!>")
            placeholderImage7.image(resizeWinningImage(f"./images/table7/{person7}"))
        elif tableHighest == 8:
            placeholder8.metric(getNameFromFilename(person8), f"{similar8}% <WIN!>")
            placeholderImage8.image(resizeWinningImage(f"./images/table8/{person8}"))
        st.balloons()

